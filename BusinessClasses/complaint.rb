require 'rubygems'
require 'sinatra'
require 'data_mapper'
require 'dm-migrations/adapters/dm-sqlite-adapter'
require 'data_mapper'
require 'dm-migrations'

class Complaint
	include DataMapper::Resource
	
	 
	property :id, Serial
	property :title, String
	property :company, String
	property :category, String 
	property :assignee, String
	property :description, String
	property :created_at, DateTime
	

	DataMapper.finalize.auto_upgrade! 
end