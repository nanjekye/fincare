require 'rubygems'
require 'sinatra'
require 'data_mapper'
require 'dm-migrations/adapters/dm-sqlite-adapter'
require 'data_mapper'
require 'dm-migrations'
require 'dm-core'
# #imports
# require '/home/joannah/FinCare/DataAcces/db'
# require '/home/joannah/FinCare/DataAccess/complaint'

DataMapper.setup(:default, "sqlite3://#{Dir.pwd}/fincare.db")
class User
	include DataMapper::Resource
	property :id, Serial

	property :name, String
	property :pass, String
end
class Complaint
	include DataMapper::Resource
	property :id, Serial
	property :title, String
	property :company, String
	property :category, String 
	property :assignee, String
	property :description, String
	property :created_at, DateTime
	

	DataMapper.finalize.auto_upgrade! 
end


get '/' do
	erb :index
end

post '/login'do 
	
	if name == "admin" && pass == "admin"
		redirect '/admin'
	elsif username == "joannah" && password == "staff"
		redirect '/staff1'

	elsif username == "richard" && password == "staff"
		redirect '/staff2'
	end
end

get '/admin' do
	erb :admin
end
post '/admin' do
	@complaint = Complaint.create(:title => params[:title], :company => params[:company], :category => params[:category],:assignee => params[:assignee],:description => params[:description],)
	if @complaint.save
		redirect '/admin'
	end
end

get '/complaints' do
	@complaints = Complaint.all
	erb :complaints
end

get '/staff1' do
	@complaints = Complaint.all(:assignee => "joannah")
	erb :staff1
end
get '/staff2' do
	@complaints = Complaint.all(:assignee => "richard")
	erb :staff2
end